package dev.mirzauksyh.todoapp;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    EditText etTodo;
    Button btAdd;
    Spinner spCategory,spFilter;
    TextView tvEmpty,tvDrag;
    String[] Category = {"Office Task","Home Task","School Task"};
    String[] Filter = {"All","Finished Task","On Going Task","Office Task","Home Task","School Task"};
    ArrayList<HashMap<String, String>> Task ;
    ArrayList<HashMap<String, String>> TaskFiltered ;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    public RecyclerViewAdapter mAdapter;
    public RecyclerViewAdapter mAdapterFiltered;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etTodo = findViewById(R.id.etTodo);
        btAdd = findViewById(R.id.btAdd);
        tvEmpty = findViewById(R.id.tvEmpty);
        tvDrag = findViewById(R.id.tvDrag);
        spCategory = findViewById(R.id.spinner);
        spFilter = findViewById(R.id.spFilter);
        recyclerView = findViewById(R.id.recyclerView);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, Category);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final ArrayAdapter<String> adapterFilter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, Filter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategory.setAdapter(adapter);
        spFilter.setAdapter(adapterFilter);
        Task  = new ArrayList<>();
        TaskFiltered  = new ArrayList<>();
        mAdapter = new RecyclerViewAdapter(Task,this);
        mAdapterFiltered = new RecyclerViewAdapter(TaskFiltered,this);
        ItemTouchHelper.Callback callback =
                new ItemMoveCallback(mAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

        recyclerView.setAdapter(mAdapter);
        progressDialog = new ProgressDialog(this);

        spFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int pos = spFilter.getSelectedItemPosition();
                TaskFiltered.clear();
                if(pos != 0) {
                    tvEmpty.setVisibility(View.GONE);
                    tvDrag.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    ItemTouchHelper.Callback callbackFiltered =
                            new ItemMoveCallback(mAdapterFiltered);
                    ItemTouchHelper touchHelperFiltered = new ItemTouchHelper(callbackFiltered);
                    touchHelperFiltered.attachToRecyclerView(recyclerView);
                    Log.e("COUNT", String.valueOf(Task.size()));
                    Log.e("POS", String.valueOf(pos));
                    for (int j = 0; j < Task.size(); j++) {
                        String stats = Task.get(j).get("stats");
                        String cat = Task.get(j).get("category");

                        if (pos == 1 && stats.equals("1")) {
                            HashMap<String, String> hashTask = new HashMap<String, String>();
                            hashTask.put("task", Task.get(j).get("task"));
                            hashTask.put("category", Task.get(j).get("category"));
                            hashTask.put("stats", Task.get(j).get("task"));
                            TaskFiltered.add(hashTask);
                        } else if (pos == 2 && stats.equals("0")) {
                            HashMap<String, String> hashTask = new HashMap<String, String>();
                            hashTask.put("task", Task.get(j).get("task"));
                            hashTask.put("category", Task.get(j).get("category"));
                            hashTask.put("stats", Task.get(j).get("task"));
                            TaskFiltered.add(hashTask);
                        } else if (pos == 3 && cat.equals("Office Task")) {
                            HashMap<String, String> hashTask = new HashMap<String, String>();
                            hashTask.put("task", Task.get(j).get("task"));
                            hashTask.put("category", Task.get(j).get("category"));
                            hashTask.put("stats", Task.get(j).get("task"));
                            TaskFiltered.add(hashTask);
                        } else if (pos == 4 && cat.equals("Home Task")) {
                            HashMap<String, String> hashTask = new HashMap<String, String>();
                            hashTask.put("task", Task.get(j).get("task"));
                            hashTask.put("category", Task.get(j).get("category"));
                            hashTask.put("stats", Task.get(j).get("task"));
                            TaskFiltered.add(hashTask);
                        } else if (pos == 5 && cat.equals("School Task")) {
                            HashMap<String, String> hashTask = new HashMap<String, String>();
                            hashTask.put("task", Task.get(j).get("task"));
                            hashTask.put("category", Task.get(j).get("category"));
                            hashTask.put("stats", Task.get(j).get("task"));
                            TaskFiltered.add(hashTask);
                        }else{
                            tvEmpty.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            tvEmpty.setText("Not Found");
                        }
                    }
                    recyclerView.setAdapter(mAdapterFiltered);
                    mAdapterFiltered.notifyDataSetChanged();
                }else{
                    if(Task.size() != 0){
                        tvEmpty.setVisibility(View.GONE);
                    }
                    recyclerView.setVisibility(View.VISIBLE);
                    recyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String task = etTodo.getText().toString();
                String cat = spCategory.getSelectedItem().toString();
                Log.e("TES","MASUK");
                if(!task.equals("")) {
                    progressDialog.setMessage("Inserting..");
                    progressDialog.show();
                    HashMap<String, String> hashTask = new HashMap<String, String>();
                    hashTask.put("task",task );
                    hashTask.put("category", cat);
                    hashTask.put("stats", "0");
                    Task.add(hashTask);

                    Log.e("ARRAY", String.valueOf(Task.size()));
                    tvEmpty.setVisibility(View.GONE);
                    etTodo.setText("");
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            progressDialog.dismiss();
                            tvDrag.setVisibility(View.VISIBLE);
                            tvEmpty.setVisibility(View.GONE);
                            mAdapter.notifyDataSetChanged();

                        }
                    }, 1500);

                }else{
                    Toast.makeText(MainActivity.this,"Task is empty",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
