package dev.mirzauksyh.todoapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> implements ItemMoveCallback.ItemTouchHelperContract {
    private ArrayList<HashMap<String,String>> data;
    Context context;
    public RecyclerViewAdapter(ArrayList<HashMap<String,String>> data,Context ctx) {
        this.data = data;
        this.context = ctx;
    }
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewAdapter.MyViewHolder holder, final int i) {
        holder.tvTask.setText(data.get(i).get("task"));
        holder.tvCategory.setText(data.get(i).get("category"));
        String status = data.get(i).get("stats");

        if(status.equals("1")){
            holder.btDone.setVisibility(View.GONE);
            holder.btUpdate.setVisibility(View.GONE);
            holder.tvFinish.setVisibility(View.VISIBLE);
        }
        holder.btUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Update");

// Set an EditText view to get user input
                final EditText input = new EditText(context);
                alert.setView(input);
                input.setText(data.get(i).get("task"));
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        HashMap<String,String> map = data.get(i);
                        String value = input.getText().toString();

                        map.put("task",value);
                        data.set(i,map);
                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("Updating..");
                        progressDialog.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                                notifyDataSetChanged();
                            }
                        }, 1500);
                    }
                });

                alert.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                               dialog.dismiss();
                            }
                        });
                alert.show();



            }
        });
        holder.btDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String,String> map = data.get(i);
                map.put("stats","1");
                data.set(i,map);
                notifyDataSetChanged();
            }
        });
        holder.btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Delete");
                alert.setMessage("Are you sure you want to delete this task ?");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        data.remove(i);
                        notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });

                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alert.show();

            }
        });
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTask;
        private TextView tvCategory;
        private TextView tvFinish;
        private TextView btUpdate;
        private TextView btDone;
        private TextView btDelete;
        View rowView;

        public MyViewHolder(View itemView) {
            super(itemView);

            rowView = itemView;
            tvTask = itemView.findViewById(R.id.txtTask);
            tvCategory = itemView.findViewById(R.id.txtCategory);
            tvFinish = itemView.findViewById(R.id.txtFinish);
            btUpdate = itemView.findViewById(R.id.btUpdate);
            btDelete = itemView.findViewById(R.id.btDelete);
            btDone = itemView.findViewById(R.id.btDone);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public void onRowMoved(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(data, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(data, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onRowSelected(MyViewHolder myViewHolder) {
//        myViewHolder.rowView.setBackgroundColor(Color.GRAY);

    }

    @Override
    public void onRowClear(MyViewHolder myViewHolder) {
//        myViewHolder.rowView.setBackgroundColor(Color.WHITE);

    }
}
